Kindle Dashboard
================
Dashboard web page for my home automation control center, optimized for Kindle e-book.

Configuration
-------------

    cp config.ini.example config.ini

Add your openweathermap API key to config parameter `apikey`.

Development
------------
Setup virtual environment:

    virtualenv -p python3 ./virtualenv
    source ./virtualenv/bin/activate
    pip install -r requirements.txt

Start:

    python dashboard.py

Open browser *http://127.0.0.1:9090*
In case of problems, look at logs in */var/log/dashboard/* and check content of weather files in */tmp/weather-current-*

Docker container
----------------

    docker build -t kindle-dashboard .
    docker run  --name kindle-dashboard kindle-dashboard

Production server
-----------------

1. Put project into */opt/kindle-dashboard*
2. Copy systemd service script from *scripts/kindle-dashboard.service* to */etc/systemd/system/*
3. Start the server: `systemctl start kindle-dashboard`
4. Open browser *http://<ip_address>:8080*
