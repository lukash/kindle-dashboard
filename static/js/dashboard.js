"use strict";

var Dashboard = function() {
    var self = this;
    var timestampDifference = 0;

    var batteryPercentageElem = document.getElementById("battery-percentage");

    var clockHoursElem = document.getElementById("clock-hours");
    var clockMinutesElem = document.getElementById("clock-minutes");

    var dateElem = document.getElementById("date-date");
    var dayofyearElem = document.getElementById("date-dayofweek");
    var yearElem = document.getElementById("date-year");

    var currentTempElem = document.getElementById("current-weather-temp");
    var currentWeatherIconElem = document.getElementById("current-weather-icon");

    var moonPhaseIconElem = document.getElementById("moon-phase");
    var sunriseTimeElem = document.getElementById("sunrise-time");
    var sunsetTimeElem = document.getElementById("sunset-time");
    var ozoneIndexElem = document.getElementById("ozone-index");

    var chartTempElem = document.getElementById("chart-temp");
    var chartCloudsElem = document.getElementById("chart-clouds");
    var chartPercipElem = document.getElementById("chart-percip");

    var prevMinutes = 0;

    this.init = function() {
        updateRootFontRem();
        updateValuesFromModel();
    };

    this.updateData = function() {
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
                if (xmlhttp.status == 200) {
                    siteData = JSON.parse(xmlhttp.responseText);
                    updateValuesFromModel();

                }
            }
        };

        xmlhttp.open("GET", "/update", true);
        xmlhttp.send();
    };

    this.updateClock = function() {
        var d = new Date(Date.now() - timestampDifference);
        var hours = numPrefix(d.getHours());
        var minutes = numPrefix(d.getMinutes());
        if (prevMinutes != d.getMinutes()) {
            clockHoursElem.textContent = hours;
            clockMinutesElem.textContent = minutes;
        }
        prevMinutes = d.getMinutes();
    };

    this.updateKindleStatus = function(kindleStatus) {
        batteryPercentageElem.textContent = kindleStatus["battery"] + "%";
    };

    function updateValuesFromModel() {
        timestampDifference = Date.now() - siteData["serverTimestamp"];

        self.updateClock();
        updateDate();
        updateCurrentWeather();
        updateCharts();
    }

    function updateDate() {
        var weekDays = ["Sun.", "Mon.", "Tue.", "Wed.", "Thu.", "Fri.", "Sat."] ;

        var d = new Date(Date.now() - timestampDifference);
        var day = numPrefix(d.getDate());
        var month =  numPrefix(d.getMonth() + 1);

        dateElem.textContent = day + "." + month + ".";
        dayofyearElem.textContent  = weekDays[d.getDay()];
        yearElem.textContent = d.getFullYear().toString();
    }

    function updateRootFontRem() {
        //var width = document.querySelector("body").offsetWidth;
        var height = document.querySelector("body").offsetHeight;
        if (height == 0) {
            height = document.querySelector("html").offsetHeight;
        }
        var root = document.querySelector(":root");
        root.style.fontSize = height / 100 + "px";
    }

    function updateCurrentWeather() {
        currentTempElem.textContent = Math.round(siteData["currentTemp"] * 10) / 10;
        currentWeatherIconElem.className = "wi " + getCurrentWeatherIcon();

        var moonPhaseIcon = getMoonIconName(getMoonPhaseForDate(new Date()));
        moonPhaseIconElem.className = "wi " + moonPhaseIcon;

        var sunriseDate = new Date(siteData["sunriseTimestamp"] * 1000);
        sunriseTimeElem.textContent = numPrefix(sunriseDate.getHours()) + ":" + numPrefix(sunriseDate.getMinutes());

        var sunsetDate = new Date(siteData["sunsetTimestamp"] * 1000);
        sunsetTimeElem.textContent = numPrefix(sunsetDate.getHours()) + ":" + numPrefix(sunsetDate.getMinutes());

        ozoneIndexElem.textContent = siteData["uvIndex"];
    }

    function getCurrentWeatherIcon() {
        var isNight = siteData["currentWeatherIcon"].slice(-1) === "n";
        if (isNight && siteData["currentWeatherIconId"] == 800) {
            // Clear night -> real moon phase icon
            return getMoonIconName(getMoonPhaseForDate(new Date()))
        }
        return "wi-owm-" + (isNight ? "night-" : "day-") + siteData["currentWeatherIconId"];
    }

    var moonIcons = [
        "wi-moon-alt-new",
        "wi-moon-alt-waxing-crescent-1",
        "wi-moon-alt-waxing-crescent-2",
        "wi-moon-alt-waxing-crescent-3",
        "wi-moon-alt-waxing-crescent-4",
        "wi-moon-alt-waxing-crescent-5",
        "wi-moon-alt-waxing-crescent-6",
        "wi-moon-alt-first-quarter",
        "wi-moon-alt-waxing-gibbous-1",
        "wi-moon-alt-waxing-gibbous-2",
        "wi-moon-alt-waxing-gibbous-3",
        "wi-moon-alt-waxing-gibbous-4",
        "wi-moon-alt-waxing-gibbous-5",
        "wi-moon-alt-waxing-gibbous-6",
        "wi-moon-alt-full",
        "wi-moon-alt-waning-gibbous-1",
        "wi-moon-alt-waning-gibbous-2",
        "wi-moon-alt-waning-gibbous-3",
        "wi-moon-alt-waning-gibbous-4",
        "wi-moon-alt-waning-gibbous-5",
        "wi-moon-alt-waning-gibbous-6",
        "wi-moon-alt-third-quarter",
        "wi-moon-alt-waning-crescent-1",
        "wi-moon-alt-waning-crescent-2",
        "wi-moon-alt-waning-crescent-3",
        "wi-moon-alt-waning-crescent-4",
        "wi-moon-alt-waning-crescent-5",
        "wi-moon-alt-waning-crescent-6"
    ];

    function getMoonPhaseForDate(date) {
        var lp = 2551443;
        var new_moon = new Date(1970, 0, 7, 20, 35, 0);
        var phase = ((date.getTime() - new_moon.getTime()) / 1000) % lp;
        return phase / lp; // 0 = new moon, 0.5 = full
    }

    function getMoonIconName(phase) {
        var iconInterval = 1 / moonIcons.length;
        return moonIcons[Math.floor(phase / iconInterval)];
    }

    function numPrefix(num) {
        return (10 > num ? "0" : "") + num;
    }

    function updateCharts() {
        chartTempElem.src = siteData["chartTempSrc"] + "?" + new Date().getTime();
        chartCloudsElem.src = siteData["chartCloudsSrc"] + "?" + new Date().getTime();
        chartPercipElem.src = siteData["chartPercipSrc"] + "?" + new Date().getTime();
    }

};

var dashboard = null;

document.addEventListener("DOMContentLoaded", function(event) {
    dashboard = new Dashboard();
    dashboard.init();
});

window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
    console.error(errorMsg);

    var data = {
        "msg": errorMsg,
        "line": lineNumber
    };

    var params = typeof data == 'string' ? data : Object.keys(data).map(
        function(k) { return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
    ).join('&');

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', "/error");
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send(params);
};

// Called by kindle-digital-dashboard
function kindleWakeupWithoutInternet(kindleStatus) {
    dashboard.updateClock();
    if (kindleStatus) {
        dashboard.updateKindleStatus(kindleStatus);
    }
}

// Called by kindle-digital-dashboard
function kindleWakeupWithInternet() {
    dashboard.updateData();
}

// Called by kindle-digital-dashboard
function kindleWatchdogPing() {
    return true;
}
