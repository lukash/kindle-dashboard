import os
import sys

cwd = os.path.dirname(__file__) + '/'

# Active the python virtualenv for this application
activate_this = cwd + 'virtualenv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

import sys
sys.path.insert(0, cwd)
from dashboard import app as application
