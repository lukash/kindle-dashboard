import os
import time
import glob
import re
import requests
import json
import configparser
import logging
import pygal
from pygal.style import Style

cwd = os.path.dirname(os.path.abspath(__file__)) + '/'
config = configparser.ConfigParser()
config.read(cwd + 'config.ini')

log = logging.getLogger('dashboard')

def update_weather_if_necessary():
    weather_file_metainfo = __weather_files_metainfo(config['DASHBOARD']['weather_file_path_template'])
    if weather_file_metainfo is None or __weather_outdated(weather_file_metainfo):
        log.info('Current weather outdated, updating...')
        weather = __fetch_weather_current(config['DASHBOARD']['openweathermap_apikey'], config['DASHBOARD']['location'])
        __store_weather_to_file(config['DASHBOARD']['weather_file_path_template'], json.dumps(weather))

    uvindex_file_metainfo = __weather_files_metainfo(config['DASHBOARD']['uvindex_file_path_template'])
    if uvindex_file_metainfo is None or __weather_outdated(uvindex_file_metainfo):
        log.info('UV index outdated, updating...')
        weather = __fetch_weather_uvindex(config['DASHBOARD']['openweathermap_apikey'], config['DASHBOARD']['location_geo'])
        __store_weather_to_file(config['DASHBOARD']['uvindex_file_path_template'], json.dumps(weather))

    forecast_file_metainfo = __weather_files_metainfo(config['DASHBOARD']['forecast_file_path_template'])
    if forecast_file_metainfo is None or __weather_outdated(forecast_file_metainfo):
        log.info('Forecast outdated, updating...')
        forecast = __fetch_weather_forecast(config['DASHBOARD']['openweathermap_apikey'], config['DASHBOARD']['location'])
        __store_weather_to_file(config['DASHBOARD']['forecast_file_path_template'], json.dumps(forecast))
        __generate_graph_images(forecast)


def load_current_weather() -> dict:
    weather_file_metainfo = __weather_files_metainfo(config['DASHBOARD']['weather_file_path_template'])

    with open(weather_file_metainfo['filename']) as f:
        content = f.read()
        return json.loads(content)


def load_current_uvindex() -> dict:
    uvindex_file_metainfo = __weather_files_metainfo(config['DASHBOARD']['uvindex_file_path_template'])

    with open(uvindex_file_metainfo['filename']) as f:
        content = f.read()
        return json.loads(content)


def __weather_files_metainfo(file_path_template: str) -> dict:
    metainfo = dict()
    file_path_regexp = re.sub('{[^}]+}', '*', file_path_template)
    found_files = glob.glob(file_path_regexp)
    newest_metainfo = None
    newest_timestamp = 0
    for f in found_files:
        metainfo['filename'] = f
        variables = __parse_variables(file_path_template, f)
        if int(variables['timestamp']) > newest_timestamp:
            newest_metainfo = {**metainfo, **variables}
            newest_timestamp = int(variables['timestamp'])

    return newest_metainfo


def __parse_variables(template: str, string: str) -> dict:
    variables = dict()
    while '{' in template:
        open_bracket_position = template.find('{')
        close_bracket_position = template.find('}')
        var_name = template[open_bracket_position + 1:close_bracket_position]
        var_value = string[open_bracket_position:close_bracket_position]
        template = template[close_bracket_position + 1:]
        variables[var_name] = var_value

    return variables 


def __weather_outdated(weather_file_metainfo: dict) -> bool:
    current_timestamp = int(time.time())
    seconds_since_update = current_timestamp - int(weather_file_metainfo['timestamp'])
    return seconds_since_update > 1800


def __fetch_weather_current(api_key: str, location: str) -> dict:
    url = config['DASHBOARD']['openweathermap_apiurl'] + '/data/2.5/weather?q={}&units=metric&appid={}'.format(location, api_key)
    r = requests.get(url, auth=(config['DASHBOARD']['weather_auth_username'], config['DASHBOARD']['weather_auth_password']))
    return r.json()


def __fetch_weather_forecast(api_key: str, location: str) -> dict:
    url = config['DASHBOARD']['openweathermap_apiurl'] + '/data/2.5/forecast?q={}&units=metric&appid={}'.format(location, api_key)
    r = requests.get(url, auth=(config['DASHBOARD']['weather_auth_username'], config['DASHBOARD']['weather_auth_password']))
    return r.json()


def __fetch_weather_uvindex(api_key: str, geo: str) -> dict:
    geo_parts = geo.split(',')
    url = config['DASHBOARD']['openweathermap_apiurl'] + '/data/2.5/uvi?appid={}&lat={}&lon={}'.format(api_key, geo_parts[0], geo_parts[1])
    r = requests.get(url, auth=(config['DASHBOARD']['weather_auth_username'], config['DASHBOARD']['weather_auth_password']))
    return r.json()


def __store_weather_to_file(file_path_template: str, content: str):
    timestamp = int(time.time())
    new_filename = file_path_template.format(timestamp=timestamp)
    with open(new_filename, 'w') as f:
        f.write(content)

    return {'timestamp': timestamp, 'filename': new_filename}


def __generate_graph_images(forecast: dict):
    __generate_graph_temp(forecast)
    __generate_graph_clouds(forecast)
    __generate_graph_percip(forecast)


def __generate_graph_temp(forecast: dict):
    chart_data = __extract_forecast_attribute(forecast, lambda x: x['main']['temp'])
    labels = list(map(lambda x: x['hour'], chart_data))
    values = list(map(lambda x: x['attribute'], chart_data))

    chart_file_path = config['DASHBOARD']['chart_file_path_template'].format(attribute='temp')

    minimal = round(min(values[0:9]), 1)
    maximal = round(max(values[0:9]), 1)
    scale_third = round((maximal - minimal) / 3, 1)
    labels_y = [minimal, round(minimal + scale_third), round(minimal + scale_third * 2, 1), maximal]

    __generate_and_store_graph('line',
                               labels[0:9],
                               labels_y,
                               values[0:9],
                               cwd + chart_file_path)


def __generate_graph_clouds(forecast: dict):
    chart_data = __extract_forecast_attribute(forecast, lambda x: x['clouds']['all'])
    labels = list(map(lambda x: x['hour'], chart_data))
    values = list(map(lambda x: x['attribute'], chart_data))

    chart_file_path = config['DASHBOARD']['chart_file_path_template'].format(attribute='clouds')
    __generate_and_store_graph('line',
                               labels[0:9],
                               [0, 25, 50, 75, 100],
                               values[0:9],
                               cwd + chart_file_path)


def __generate_graph_percip(forecast: dict):
    chart_data = __extract_forecast_attribute(forecast, lambda x: x['rain']['3h'] if 'rain' in x and '3h' in x['rain'] else 0)
    labels = list(map(lambda x: x['hour'], chart_data))
    values = list(map(lambda x: x['attribute'], chart_data))

    chart_file_path = config['DASHBOARD']['chart_file_path_template'].format(attribute='percip')
    __generate_and_store_graph('bar',
                               labels[0:9],
                               [0, 1, 2, 3, 4, 5],
                               values[0:9],
                               cwd + chart_file_path)


def __extract_forecast_attribute(forecast, extract_attribue_fn) -> list:
    sorted_forecast = sorted(forecast['list'], key=lambda x: x['dt'])
    extract_fn = lambda x: {'hour': time.localtime(x['dt']).tm_hour, 'attribute': extract_attribue_fn(x)}
    chart_data = map(extract_fn, sorted_forecast)
    return list(chart_data)


def __generate_and_store_graph(graph_type, labels_x, labels_y, values, file_path):
    custom_style = Style(
        background='transparent',
        plot_background='transparent',
        foreground='#262626',
        foreground_strong='#FFFFF',
        foreground_subtle='#262626',
        opacity='1.0',
        colors=('#000000', '#E8537A', '#E95355', '#E87653', '#E89B53'),
        label_font_size=26,
        major_label_font_size=26,
        guide_stroke_dasharray='4,4',
        major_guide_stroke_dasharray='4,4',
        stroke_width='5'
    )

    try:
        if graph_type == 'line':
            graph = pygal.Line(interpolate='hermite', show_legend=False, style=custom_style, width=500, height=170, max_scale=6, human_readable=True, include_x_axis=False, margin=0, margin_top=10)
        else:
            graph = pygal.Bar(interpolate='hermite', show_legend=False, style=custom_style, width=500, height=170, max_scale=6, human_readable=True, include_x_axis=True, margin=0, margin_top=10)

        graph.x_labels = labels_x
        graph.y_labels = labels_y
        graph.add('temp',  values, show_dots=False)

        graph.render_to_file(file_path)
    except Exception as e:
        log.error('Failed to generate graph file', e)
        raise e


if __name__ == '__main__':
    # with open('/tmp/weather-forecast-1567234019.json') as f:
    #    content = f.read()
    #    forecast = json.loads(content)
    #    __generate_graph_images(forecast)
    update_weather_if_necessary()
