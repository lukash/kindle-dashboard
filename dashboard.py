import time
import os
import json
import logging
import configparser

import asyncio
import aiohttp
from aiohttp import web
import aiohttp_jinja2
import jinja2
from aiohttp_jinja2 import render_template

from dashboard_weather_charts import update_weather_if_necessary, load_current_weather, load_current_uvindex

cwd = os.path.dirname(os.path.abspath(__file__)) + '/'
config = configparser.ConfigParser()
config.read(cwd + 'config.ini')

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=config['DASHBOARD']['log'],
                    filemode='w')
logging.getLogger('dashboard').addHandler(logging.StreamHandler())
log = logging.getLogger('dashboard')

msg_queue = []
msg_counter = 1
ws_connection = None
routes = web.RouteTableDef()


@routes.get('/')
async def main(request):
    site_data = load_site_data(request)
    return render_template('main.html', request, site_data)


@routes.get('/dashboard')
async def dashboard(request):
    site_data = load_site_data(request)
    return render_template('dashboard.html', request, site_data)


@routes.get('/floorplan')
async def floorplan(request):
    site_data = load_site_data(request)
    return render_template('floorplan.html', request, site_data)


@routes.get('/update')
async def update(request):
    site_data = load_site_data(request)
    return web.json_response(site_data)


@routes.get('/hass/events')
async def evnts(request):
    messages = []
    size = msg_queue.qsize()
    if size > 0:
        while size > 0:
            msg = await asyncio.wait_for(msg_queue.get(), timeout=10)
            messages.append(json.loads(msg))
            size -= 1
    else:
        msg = await asyncio.wait_for(msg_queue.get(), timeout=10)
        messages.append(json.loads(msg))

    return web.json_response(messages)


@routes.post('/hass/message')
async def message(request):
    global msg_counter
    request_data = await request.json()
    msg = {
        "id": msg_counter,
        "type": "call_service",
        "domain": "switch",
        "service": request_data['action'],
        "service_data": {
            "entity_id": request_data['entity_id']
        }
    }
    await send_message_to_websocket(msg)
    msg_counter += 1

    return web.json_response({})


@routes.post('/error')
async def error(request):
    form_data = await request.post()
    msg = form_data['msg']
    line = form_data['line']

    log.error('JavaScript error: "%s", lineNo: "%s"' % (msg, line))

    return ''


def load_site_data(request):
    update_weather_if_necessary()
    current_weather = load_current_weather()
    current_uvindex = load_current_uvindex()

    siteData = {
        'widthPx': request.rel_url.query.get('widthPx', 1024),
        'heightPx': request.rel_url.query.get('heightPx', 768),
        'serverTimestamp': int(time.time() * 1000),
        'currentTemp': current_weather['main']['temp'],
        'currentWeatherIconId': current_weather['weather'][0]['id'],
        'currentWeatherIcon': current_weather['weather'][0]['icon'],
        'sunriseTimestamp': current_weather['sys']['sunrise'],
        'sunsetTimestamp': current_weather['sys']['sunset'],
        'uvIndex': int(current_uvindex['value']),
        'chartTempSrc': '/static/weather-chart-temp.svg',
        'chartCloudsSrc': '/static/weather-chart-clouds.svg',
        'chartPercipSrc': '/static/weather-chart-percip.svg'
    }

    return siteData

async def connect_to_home_assistant():
    global messages
    global ws_connection
    log.info('Connecting to home assistant web socket')
    async with aiohttp.ClientSession() as session:
        async with session.ws_connect(config['DASHBOARD']['hass_websocket_url']) as ws:
            ws_connection = ws
            log.info('Connected to home assistant web socket')
            await send_message_to_websocket({'type': 'auth',
                                       'access_token': config['DASHBOARD']['hass_access_token']})

            # Register to HAAS events:
            # await send_message_to_websocket({'id': 1, 'type': 'subscribe_events', 'event_type': 'state_changed'})
            async for msg in ws:
                log.debug('Message received: ' + str(msg))
                if msg.type == aiohttp.WSMsgType.TEXT:
                    msg_queue.append(msg.data)
                elif msg.type == aiohttp.WSMsgType.ERROR:
                    log.error(f'WebSocket connection closed with exception: {ws.exception()}')


async def send_message_to_websocket(request: dict):
    message = json.dumps(request)
    if ws_connection:
        await ws_connection.send_str(message)
    else:
        log.error('WebSocket connection is not established')


async def start_background_tasks(app):
    app['websocket_listener'] = asyncio.create_task(connect_to_home_assistant())


async def cleanup_background_tasks(app):
    app['websocket_listener'].cancel()
    await app['websocket_listener']


async def create_aiohttp_server():
    app = web.Application()
    aiohttp_jinja2.setup(app,
        loader=jinja2.FileSystemLoader('templates'))
    app.add_routes(routes)
    app['static_root_url'] = '/static'
    app.router.add_static('/static/', path='./static/', name='static')

    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)

    return app


if __name__ == '__main__':
    app = create_aiohttp_server()
    web.run_app(app, port=9090)
