FROM python:3.12.3-alpine3.19

EXPOSE 9090

RUN adduser -D -g '' dashboard
USER dashboard

WORKDIR /usr/src/app

COPY --chown=dashboard:dashboard requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY --chown=dashboard:dashboard . .

CMD [ "python", "./dashboard.py" ]
